package ru.ilinovsg.tm.controller.impl;

import ru.ilinovsg.tm.controller.AccountController;
import ru.ilinovsg.tm.dto.AccountDTO;
import ru.ilinovsg.tm.dto.AccountListResponseDTO;
import ru.ilinovsg.tm.dto.AccountResponseDTO;
import ru.ilinovsg.tm.service.AccountService;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.ilinovsg.tm.controller.AccountController")
public class AccountControllerImpl implements AccountController {
    private AccountService accountService;

    public AccountControllerImpl() {
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public AccountResponseDTO createAccount(AccountDTO accountDTO) {
        return accountService.createAccount(accountDTO);
    }

    @Override
    public AccountResponseDTO updateAccount(AccountDTO accountDTO) {
        return accountService.updateAccount(accountDTO);
    }

    @Override
    public AccountResponseDTO deleteAccount(Long id) {
        return accountService.deleteAccount(id);
    }

    @Override
    public AccountResponseDTO getAccount(Long id) {
        return accountService.getAccount(id);
    }

    @Override
    public AccountListResponseDTO getAllAccounts() {
        return accountService.getAllAccounts();
    }
}
