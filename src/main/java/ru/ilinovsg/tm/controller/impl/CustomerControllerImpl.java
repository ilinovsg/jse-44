package ru.ilinovsg.tm.controller.impl;

import lombok.extern.log4j.Log4j2;
import ru.ilinovsg.tm.controller.CustomerController;
import ru.ilinovsg.tm.dto.CustomerDTO;
import ru.ilinovsg.tm.dto.CustomerListResponseDTO;
import ru.ilinovsg.tm.dto.CustomerResponseDTO;
import ru.ilinovsg.tm.service.CustomerService;
import ru.ilinovsg.tm.util.InstantConverter;

import javax.jws.WebService;
import java.util.Date;

@Log4j2
@WebService(endpointInterface = "ru.ilinovsg.tm.controller.CustomerController")
public class CustomerControllerImpl implements CustomerController {
    private CustomerService customerService;

    public CustomerControllerImpl() {
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public CustomerResponseDTO createCustomer(CustomerDTO customerDTO) {
        return customerService.createCustomer(customerDTO);
    }

    @Override
    public CustomerResponseDTO updateCustomer(CustomerDTO customerDTO) {
        return customerService.updateCustomer(customerDTO);
    }

    @Override
    public CustomerResponseDTO deleteCustomer(Long id) {
        return customerService.deleteCustomer(id);
    }

    @Override
    public CustomerResponseDTO getCustomer(Long id) { return customerService.getCustomer(id);}

    @Override
    public CustomerListResponseDTO getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @Override
    public CustomerListResponseDTO findByBirthDate(Date birthDate) {
        return customerService.findByBirthDate(birthDate);
    }

    @Override
    public CustomerListResponseDTO findByName(String name) {
        return customerService.findByName(name);
    }

    @Override
    public CustomerListResponseDTO findByAccountNumber(String accountNumber) {
        return customerService.findByAccountNumber(accountNumber);
    }
}
