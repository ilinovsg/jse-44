package ru.ilinovsg.tm.controller;

import ru.ilinovsg.tm.dto.CustomerDTO;
import ru.ilinovsg.tm.dto.CustomerListResponseDTO;
import ru.ilinovsg.tm.dto.CustomerResponseDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.Date;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface CustomerController {
    @WebMethod
    CustomerResponseDTO createCustomer(CustomerDTO CustomerDTO);

    @WebMethod
    CustomerResponseDTO updateCustomer(CustomerDTO CustomerDTO);

    @WebMethod
    CustomerResponseDTO deleteCustomer(Long id);

    @WebMethod
    CustomerResponseDTO getCustomer(Long id);

    @WebMethod
    CustomerListResponseDTO getAllCustomers();

    @WebMethod
    CustomerListResponseDTO findByBirthDate(Date birthDate);

    @WebMethod
    CustomerListResponseDTO findByName(String name);

    @WebMethod
    CustomerListResponseDTO findByAccountNumber(String accountNumber);
}

