package ru.ilinovsg.tm.controller;

import ru.ilinovsg.tm.dto.AccountDTO;
import ru.ilinovsg.tm.dto.AccountListResponseDTO;
import ru.ilinovsg.tm.dto.AccountResponseDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AccountController {
    @WebMethod
    AccountResponseDTO createAccount(AccountDTO AccountDTO);

    @WebMethod
    AccountResponseDTO updateAccount(AccountDTO AccountDTO);

    @WebMethod
    AccountResponseDTO deleteAccount(Long id);

    @WebMethod
    AccountResponseDTO getAccount(Long id);

    @WebMethod
    AccountListResponseDTO getAllAccounts();
}
