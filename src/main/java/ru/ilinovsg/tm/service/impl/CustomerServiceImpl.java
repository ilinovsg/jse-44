package ru.ilinovsg.tm.service.impl;

import ru.ilinovsg.tm.dto.CustomerDTO;
import ru.ilinovsg.tm.dto.CustomerListResponseDTO;
import ru.ilinovsg.tm.dto.CustomerResponseDTO;
import ru.ilinovsg.tm.enums.Status;
import ru.ilinovsg.tm.mapper.CustomerMapper;
import ru.ilinovsg.tm.model.Customer;
import ru.ilinovsg.tm.repository.CustomerRepository;
import ru.ilinovsg.tm.service.CustomerService;
import ru.ilinovsg.tm.util.InstantConverter;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;


public class CustomerServiceImpl implements CustomerService {
    private static CustomerServiceImpl instance = null;
    private final CustomerRepository customerRepository;

    private CustomerServiceImpl() {
        customerRepository = null;
    }

    private CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public static CustomerServiceImpl getInstance(CustomerRepository customerRepository) {
        if (instance == null) {
            synchronized (CustomerServiceImpl.class) {
                if (instance == null) {
                    instance = new CustomerServiceImpl(customerRepository);
                }
            }
        }
        return instance;
    }

    @Override
    public CustomerResponseDTO createCustomer(CustomerDTO customerDTO) {
        Customer customer = Customer.builder()
                .id(customerDTO.getId())
                .firstName(customerDTO.getFirstName())
                .lastName(customerDTO.getLastName())
                .birthDate(customerDTO.getBirthDate())
                .build();
        Optional<Customer> customerOptional = customerRepository.create(customer);
        if (customerOptional.isPresent()) {
            return CustomerResponseDTO.builder().payload(CustomerMapper.toDto(customerOptional.get())).status(Status.OK).build();
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public CustomerResponseDTO updateCustomer(CustomerDTO customerDTO) {
        Customer customer = Customer.builder()
                .id(customerDTO.getId())
                .firstName(customerDTO.getFirstName())
                .lastName(customerDTO.getLastName())
                .birthDate(customerDTO.getBirthDate())
                .build();
        Optional<Customer> customerOptional = customerRepository.update(customer);
        if (customerOptional.isPresent()) {
            return CustomerResponseDTO.builder().payload(CustomerMapper.toDto(customerOptional.get())).status(Status.OK).build();
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public CustomerResponseDTO deleteCustomer(Long id) {
        customerRepository.delete(id);
        return CustomerResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public CustomerResponseDTO getCustomer(Long id) {
        Optional<Customer> customerOptional = customerRepository.getById(id);
        if (customerOptional.isPresent()) {
            return CustomerResponseDTO.builder().payload(CustomerMapper.toDto(customerOptional.get())).status(Status.OK).build();
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public CustomerListResponseDTO getAllCustomers() {
        List<Customer> customers = customerRepository.getAll();
        CustomerDTO[] customersArray = customers.stream().map(customer -> CustomerMapper.toDto(customer)).toArray(CustomerDTO[]::new);
        return CustomerListResponseDTO.builder().status(Status.OK).payload(customersArray).build();
    }

    @Override
    public CustomerListResponseDTO findByBirthDate(Date birthDate) {
        List<Customer> customers = customerRepository.findCustomers(null, null, birthDate, null, null);
        CustomerDTO[] customersArray = customers.stream().map(customer -> CustomerMapper.toDto(customer)).toArray(CustomerDTO[]::new);
        return CustomerListResponseDTO.builder().status(Status.OK).payload(customersArray).build();
    }

    @Override
    public CustomerListResponseDTO findByName(String firstName) {
        List<Customer> customers = customerRepository.findCustomers(firstName, null, null, null, null);
        CustomerDTO[] customersArray = customers.stream().map(customer -> CustomerMapper.toDto(customer)).toArray(CustomerDTO[]::new);
        return CustomerListResponseDTO.builder().status(Status.OK).payload(customersArray).build();
    }

    @Override
    public CustomerListResponseDTO findByAccountNumber(String accountNumber) {
        List<Customer> customers = customerRepository.findCustomers(null, null, null, accountNumber, null);
        CustomerDTO[] customersArray = customers.stream().map(customer -> CustomerMapper.toDto(customer)).toArray(CustomerDTO[]::new);
        return CustomerListResponseDTO.builder().status(Status.OK).payload(customersArray).build();
    }
}
