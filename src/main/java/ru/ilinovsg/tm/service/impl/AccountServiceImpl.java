package ru.ilinovsg.tm.service.impl;

import ru.ilinovsg.tm.dto.*;
import ru.ilinovsg.tm.enums.Status;
import ru.ilinovsg.tm.mapper.AccountMapper;
import ru.ilinovsg.tm.mapper.CustomerMapper;
import ru.ilinovsg.tm.model.Account;
import ru.ilinovsg.tm.model.Customer;
import ru.ilinovsg.tm.repository.AccountRepository;
import ru.ilinovsg.tm.service.AccountService;
import ru.ilinovsg.tm.util.InstantConverter;

import java.util.List;
import java.util.Optional;

public class AccountServiceImpl implements AccountService {
    private static AccountServiceImpl instance = null;
    private final AccountRepository accountRepository;

    private AccountServiceImpl() {
        accountRepository = null;
    }

    private AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public static AccountServiceImpl getInstance(AccountRepository accountRepository) {
        if (instance == null) {
            synchronized (AccountServiceImpl.class) {
                if (instance == null) {
                    instance = new AccountServiceImpl(accountRepository);
                }
            }
        }
        return instance;
    }

    @Override
    public AccountResponseDTO createAccount(AccountDTO accountDTO) {
        Account account = Account.builder()
                .customerId(accountDTO.getCustomerId())
                .accountNumber(accountDTO.getAccountNumber())
                .balance(accountDTO.getBalance())
                .build();
        Optional<Account> accountOptional = accountRepository.create(account);
        if (accountOptional.isPresent()) {
            return AccountResponseDTO.builder().payload(AccountMapper.toDto(accountOptional.get())).status(Status.OK).build();
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public AccountResponseDTO updateAccount(AccountDTO accountDTO) {
        Account account = Account.builder()
                .customerId(accountDTO.getCustomerId())
                .accountNumber(accountDTO.getAccountNumber())
                .balance(accountDTO.getBalance())
                .build();
        Optional<Account> accountOptional = accountRepository.update(account);
        if (accountOptional.isPresent()) {
            return AccountResponseDTO.builder().payload(AccountMapper.toDto(accountOptional.get())).status(Status.OK).build();
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public AccountResponseDTO deleteAccount(Long id) {
        accountRepository.delete(id);
        return AccountResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public AccountResponseDTO getAccount(Long id) {
        Optional<Account> accountOptional = accountRepository.getById(id);
        if (accountOptional.isPresent()) {
            return AccountResponseDTO.builder().payload(AccountMapper.toDto(accountOptional.get())).status(Status.OK).build();
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public AccountListResponseDTO getAllAccounts() {
        List<Account> accounts = accountRepository.getAll();
        AccountDTO[] accountsArray = accounts.stream().map(account -> AccountMapper.toDto(account)).toArray(AccountDTO[]::new);
        return AccountListResponseDTO.builder().status(Status.OK).payload(accountsArray).build();
    }
}
