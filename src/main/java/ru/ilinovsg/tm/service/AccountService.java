package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.dto.*;

public interface AccountService {
    AccountResponseDTO createAccount(AccountDTO accountDTO);

    AccountResponseDTO updateAccount(AccountDTO accountDTO);

    AccountResponseDTO deleteAccount(Long id);

    AccountResponseDTO getAccount(Long id);

    AccountListResponseDTO getAllAccounts();
}
