package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.dto.CustomerDTO;
import ru.ilinovsg.tm.dto.CustomerListResponseDTO;
import ru.ilinovsg.tm.dto.CustomerResponseDTO;

import java.util.Date;

public interface CustomerService {
    CustomerResponseDTO createCustomer(CustomerDTO customerDTO);

    CustomerResponseDTO updateCustomer(CustomerDTO customerDTO);

    CustomerResponseDTO deleteCustomer(Long id);

    CustomerResponseDTO getCustomer(Long id);

    CustomerListResponseDTO getAllCustomers();

    CustomerListResponseDTO findByBirthDate(Date birthDate);

    CustomerListResponseDTO findByName(String name);

    CustomerListResponseDTO findByAccountNumber(String accountNumber);
}
