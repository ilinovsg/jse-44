package ru.ilinovsg.tm.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Customer extends AbstractEntity {
    private BigInteger id;
    private String firstName;
    private String lastName;
    private Date birthDate;

    private String accountNumber;
    private BigDecimal balance;
}
