package ru.ilinovsg.tm.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

public abstract class AbstractEntity {
    @Getter
    @Setter
    BigInteger id;
}
