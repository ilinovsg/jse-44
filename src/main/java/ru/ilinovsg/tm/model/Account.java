package ru.ilinovsg.tm.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account extends AbstractEntity {
    private BigInteger customerId;
    private String accountNumber;
    private BigDecimal balance;
}
