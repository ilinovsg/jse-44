package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.dto.AccountDTO;
import ru.ilinovsg.tm.model.Account;

import java.util.Optional;

public interface AccountRepository extends Repository<Account> {
}
