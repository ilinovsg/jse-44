package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface Repository<T extends AbstractEntity> {
    Optional<T> create(T input);

    Optional<T> update(T input);

    Optional<T> getById(Long id);

    void delete(Long id);

    List<T> getAll();
}
