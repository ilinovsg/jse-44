package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.model.Customer;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public interface CustomerRepository extends Repository<Customer> {
    List<Customer> findCustomers(String firstName, String lastName, Date birthDate, String accountNumber, BigInteger balance);
}
