package ru.ilinovsg.tm.repository.impl;

import org.apache.ibatis.session.SqlSession;
import ru.ilinovsg.tm.model.Account;
import ru.ilinovsg.tm.repository.AccountRepository;
import ru.ilinovsg.tm.repository.datasource.MyBatisUtil;
import ru.ilinovsg.tm.repository.impl.mapper.AccountMBMapper;

import java.util.List;
import java.util.Optional;

public class AccountRepositoryImpl implements AccountRepository {
    private static volatile AccountRepositoryImpl instance = null;

    private AccountRepositoryImpl() {
    }

    public static AccountRepositoryImpl getInstance() {
        if (instance == null) {
            synchronized (AccountRepositoryImpl.class) {
                if (instance == null) {
                    instance = new AccountRepositoryImpl();
                }
            }
        }
        return instance;
    }

    @Override
    public Optional<Account> create(Account input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.save(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Account> update(Account input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.updateAccount(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Account> getById(Long id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            return Optional.ofNullable(accountMBMapper.getById(id));
        }
    }

    @Override
    public void delete(Long id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.deleteAccountById(id);
            sqlSession.commit();
        }
    }

    @Override
    public List<Account> getAll() {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            AccountMBMapper mapper = session.getMapper(AccountMBMapper.class);
            return mapper.getAllAccounts();
        }
    }
}
