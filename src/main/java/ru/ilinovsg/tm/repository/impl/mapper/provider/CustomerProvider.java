package ru.ilinovsg.tm.repository.impl.mapper.provider;

import org.apache.ibatis.jdbc.SQL;

import java.math.BigDecimal;
import java.util.Date;

public class CustomerProvider {
    public String findCustomers(String firstName, String lastName, Date birthDate, String accountNumber, BigDecimal balance) {
        return new SQL() {{
            SELECT("c.first_Name", "c.last_Name", "c.birth_Date", "a.account_number", "a.ballance");
            FROM("customer c");
            JOIN("account a on a.customer_id=c.id");
            boolean hasCondition = false;
            if (firstName != null) {
                WHERE("c.first_Name like '%" + firstName + "%'");
                hasCondition = true;
            }
            if (lastName != null) {
                if (hasCondition) {
                    AND();
                }
                WHERE("c.last_Name like '%" + lastName + "%'");
                hasCondition = true;
            }
            if (birthDate != null) {
                if (hasCondition) {
                    AND();
                }
                WHERE("c.birth_Date='" + birthDate + "'");
                hasCondition = true;
            }
            if (accountNumber != null) {
                if (hasCondition) {
                    AND();
                }
                WHERE("account_number='" + accountNumber + "'");
                hasCondition = true;
            }
            if (balance != null) {
                if (hasCondition) {
                    AND();
                }
                WHERE("ballance='" + balance + "'");
            }

        }}.toString();
    }
}
