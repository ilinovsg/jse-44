package ru.ilinovsg.tm.repository.impl.mapper;

import org.apache.ibatis.annotations.*;
import ru.ilinovsg.tm.model.Account;

import java.util.List;

public interface AccountMBMapper {
    @Insert("insert into account(customer_id, account_number, ballance) values (#{customerId}, #{accountNumber}, #{balance})")
    Integer save(Account account);

    @Update("update account set account_number = #{accountNumber}, ballance = #{balance} where account_number = #{accountNumber}")
    void updateAccount(Account account);

    @Delete("delete from account where customer_id = #{id}")
    void deleteAccountById(Long id);

    @Select("select customer_id, account_number, ballance FROM account where customer_id = #{id}")
    @Results(value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "accountNumber", column = "account_number"),
            @Result(property = "balance", column = "ballance")
    })
    Account getById(Long id);

    @Select("SELECT customer_id, account_number, ballance FROM account")
    @Results(value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "accountNumber", column = "account_number"),
            @Result(property = "balance", column = "ballance"),
    })
    List<Account> getAllAccounts();
}
