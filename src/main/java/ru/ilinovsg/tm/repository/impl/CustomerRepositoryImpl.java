package ru.ilinovsg.tm.repository.impl;

import org.apache.ibatis.session.SqlSession;
import ru.ilinovsg.tm.model.Customer;
import ru.ilinovsg.tm.repository.CustomerRepository;
import ru.ilinovsg.tm.repository.datasource.MyBatisUtil;
import ru.ilinovsg.tm.repository.impl.mapper.CustomerMBMapper;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class CustomerRepositoryImpl implements CustomerRepository {
    private static volatile CustomerRepositoryImpl instance = null;

    private CustomerRepositoryImpl() {
    }

    public static CustomerRepositoryImpl getInstance() {
        if (instance == null) {
            synchronized (CustomerRepositoryImpl.class) {
                if (instance == null) {
                    instance = new CustomerRepositoryImpl();
                }
            }
        }
        return instance;
    }

    @Override
    public Optional<Customer> create(Customer input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.save(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Customer> update(Customer input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.updateCustomer(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Customer> getById(Long id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return Optional.ofNullable(customerMBMapper.getById(id));
        }
    }

    @Override
    public void delete(Long id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.deleteCustomerById(id);
            sqlSession.commit();
        }
    }

    @Override
    public List<Customer> getAll() {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return customerMBMapper.getAllCustomers();
        }
    }

    @Override
    public List<Customer> findCustomers(String firstName, String lastName, Date birthDate, String accountNumber, BigInteger balance) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return customerMBMapper.findCustomers(firstName, lastName, birthDate, accountNumber, balance);
        }
    }
}
