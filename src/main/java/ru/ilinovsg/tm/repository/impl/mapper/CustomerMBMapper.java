package ru.ilinovsg.tm.repository.impl.mapper;

import org.apache.ibatis.annotations.*;
import ru.ilinovsg.tm.model.Customer;
import ru.ilinovsg.tm.repository.impl.mapper.provider.CustomerProvider;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public interface CustomerMBMapper {
    @Insert("insert into customer (first_Name, last_Name, birth_Date) values (#{firstName}, #{lastName}, #{birthDate})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer save(Customer customer);

    @Update("update customer set first_Name = #{firstName}, last_Name = #{lastName}, birth_Date = #{birthDate} where id = #{id}")
    void updateCustomer(Customer customer);

    @Delete("delete from customer where id = #{id}")
    void deleteCustomerById(Long id);

    @Select("select * from customer where id = #{id}")
    @Results({
            @Result(property = "firstName", column = "first_Name"),
            @Result(property = "lastName", column = "last_Name"),
            @Result(property = "birthDate", column = "birth_Date")
    })
    Customer getById(Long id);

    @Select("select * from customer")
    @Results({
            @Result(property = "firstName", column = "first_Name"),
            @Result(property = "lastName", column = "last_Name"),
            @Result(property = "birthDate", column = "birth_Date")
    })
    List<Customer> getAllCustomers();

    @SelectProvider(type = CustomerProvider.class, method = "findCustomers")
    @Results({
            @Result(property = "firstName", column = "first_Name"),
            @Result(property = "lastName", column = "last_Name"),
            @Result(property = "birthDate", column = "birth_Date"),

            @Result(property = "accountNumber", column = "account_number"),
            @Result(property = "balance", column = "ballance"),
    })
    List<Customer> findCustomers(String firstName, String lastName, Date birthDate, String accountNumber, BigInteger balance);
}
