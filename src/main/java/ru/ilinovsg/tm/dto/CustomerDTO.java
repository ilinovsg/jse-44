package ru.ilinovsg.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ilinovsg.tm.model.Account;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDTO {
    public static final Long serialVersionUID = 1L;

    private BigInteger id;
    private String firstName;
    private String lastName;
    private Date birthDate;

    private String accountNumber;
    private BigDecimal balance;
}
