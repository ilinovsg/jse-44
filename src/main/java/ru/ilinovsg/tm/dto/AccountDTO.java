package ru.ilinovsg.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO {
    public static final Long serialVersionUID = 1L;

    private BigInteger customerId;
    private String accountNumber;
    private BigDecimal balance;
    private List<CustomerDTO> customers;
}
