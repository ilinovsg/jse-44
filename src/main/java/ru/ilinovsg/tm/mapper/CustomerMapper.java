package ru.ilinovsg.tm.mapper;

import ru.ilinovsg.tm.dto.CustomerDTO;
import ru.ilinovsg.tm.model.Customer;


public class CustomerMapper {
    public static CustomerDTO toDto(Customer customer) {
        return CustomerDTO.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .birthDate(customer.getBirthDate())
                .accountNumber(customer.getAccountNumber())
                .balance(customer.getBalance())
                .build();
    }
}
