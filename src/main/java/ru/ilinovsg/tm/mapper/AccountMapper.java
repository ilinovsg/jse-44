package ru.ilinovsg.tm.mapper;

import ru.ilinovsg.tm.dto.AccountDTO;
import ru.ilinovsg.tm.model.Account;

public class AccountMapper {
    public static AccountDTO toDto(Account account) {
        return AccountDTO.builder()
                .customerId(account.getCustomerId())
                .accountNumber(account.getAccountNumber())
                .balance(account.getBalance())
                .build();
    }
}
