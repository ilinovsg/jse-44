package ru.ilinovsg.tm;

import ru.ilinovsg.tm.controller.impl.AccountControllerImpl;
import ru.ilinovsg.tm.controller.impl.CustomerControllerImpl;
import ru.ilinovsg.tm.repository.impl.AccountRepositoryImpl;
import ru.ilinovsg.tm.repository.impl.CustomerRepositoryImpl;
import ru.ilinovsg.tm.service.impl.CustomerServiceImpl;
import ru.ilinovsg.tm.service.impl.AccountServiceImpl;

import javax.xml.ws.Endpoint;

public class Main {
    public static void main(String[] args) {
        CustomerControllerImpl customerController = new CustomerControllerImpl();
        customerController.setCustomerService(CustomerServiceImpl.getInstance(CustomerRepositoryImpl.getInstance()));
        Endpoint.publish("http://localhost:8899/ws/customer", customerController);


        AccountControllerImpl accountController = new AccountControllerImpl();
        accountController.setAccountService(AccountServiceImpl.getInstance(AccountRepositoryImpl.getInstance()));
        Endpoint.publish("http://localhost:8899/ws/account", accountController);

    }
}

