package ru.ilinovsg.tm.enums;

import java.util.HashMap;
import java.util.Map;

public enum Action {
    EXIT("exit"),
    CREATE_CUSTOMER("createCustomer"),
    UPDATE_CUSTOMER("updateCustomer"),
    DELETE_CUSTOMER("deleteCustomer"),
    GET_CUSTOMER("getCustomer"),
    GET_CUSTOMER_BY_START("getCustomerByStart"),
    GET_CUSTOMER_BY_NAME("getCustomerByName"),
    GET_ALL_CUSTOMERS("getAllCustomers"),
    EMPTY("");

    private final static Map<String, Action> map = new HashMap<>();

    static {
        for (Action action1 : Action.values()) {
            map.put(action1.getAction(), action1);
        }
    }

    private String action;

    Action(String action) {
        this.action = action;
    }

    public static Action findAction(String action) {
        return map.get(action);
    }

    public String getAction() {
        return action;
    }
}
